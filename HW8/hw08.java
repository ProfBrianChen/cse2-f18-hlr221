//////////////
//// Hannah Roach
//// 11/10/18
//// CSE002 HW8 Arrays
///
//// Write a code for the three methods: shuffle(list), getHands(list, index, numCards), and printArray(list)
//// you're given a deck of 52 cards, represented by the array cards of Strings. 
//// print out all the cards in the deck
//// shuffle the whole deck of cards
//// print out the shuffled deck
//// get a hand of cards from the shuffled deck
//// print out the hand

import java.util.Scanner;
import java.util.Random;
public class hw08{ 
public static void main(String[] args) { 
  Scanner scan = new Scanner(System.in); 

  //suits club, heart, spade or diamond 
  String[] suitNames={"C","H","S","D"};    
  String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"}; 
  String[] cards = new String[52]; 
  String[] hand = new String[5]; 
  int numCards = 1; 
  int again = 1; 
  int index = 51;
  
  for (int i = 0; i < 52; i++){ 
    cards[i] = rankNames[i % 13] + suitNames[i / 13]; 
    System.out.print(cards[i] + " "); 
  } 
  
  System.out.println();
  printArray(cards); 
  shuffle(cards); 
  printArray(cards); 
  
  while(again == 1){ 
    hand = getHand(cards,index,numCards); 
    printArray(hand);
    index = index - numCards;
    System.out.println();
    System.out.println("Enter a 1 if you want another hand drawn"); 
    again = scan.nextInt(); 
  }  
  
  } // end of main method



//// printArray() takes an array of Strings and prints out each element, separated by a space " "
public static void printArray(String[] list) {
  for (int i = 0; i < list.length; i++) {
    System.out.print(list[i] + " ");
  }

} // end of printArray method



//// shuffle() shuffles the elements of the list by 
//// continuously randomizing a non-zero index number 
//// and swaps the element at the index with the first element (at index zero)
//// swap > 50 times
public static String[] shuffle(String[] list){
  int count = 0;
  int x = 0;
  int y = 0;
  while(count < 50) {
    x = (int)(Math.random()*52 + 1);
    list[y] = list[x];
    y++;
    count++; 
  }
  return list; 
} // end of shuffle method



//// getHands() returns an array that holds the # of cards in numCards.
//// cards should be taken off at then end of the list of cards.
public static String[] getHand(String[] list, int index, int numCards) {
  //numCards  = the number of cards we want to take off of our list of cards at the end 
  String[] newArray = new String[numCards];
  int count = 0;
  while (count < numCards) {
    for (int i = list.length; i > 0; i--) {
      newArray[i] = list[i];
      //System.out.println("Hello");
    }
    count++;
  }
  return newArray;
} // end of getHand method



} // end of public class
