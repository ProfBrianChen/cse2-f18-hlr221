//////////////
//// CSE 02 Welcome
///
public class WelcomeClass{
  
  public static void main(String args[]){
    ///prints Welcome to terminal window
    System.out.println("    -----------    ");
    System.out.println("    | WELCOME |    ");
    System.out.println("    -----------    ");
    System.out.println("  ^  ^  ^  ^  ^  ^  ");
    System.out.println("// \// \// \// \// \");
    System.out.println("<-E--J--K--0--0--0->");
    System.out.println("\ //\ //\ //\ //\ //\ //");
    System.out.println("  v  v  v  v  v  v  ");
    
  }
}
        