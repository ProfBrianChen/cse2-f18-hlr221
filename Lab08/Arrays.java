//////////////
//// Hannah Roach
//// 11/8/18
//// CSE002 Lab08 Arrays
///
//// 1. Create 2 arrays of integers of size 100
//// 2. Fill in the first array with randomized intergers 0 to 99 & use Math.random().
//// 3. Use the second array to hold the number or occurrences of each number in the first array.

import java.util.Random;

public class Arrays{
  public static void main(String[]args) {
    
    int [] array1 = new int[100];
    int [] array2 = new int[100];
    
    System.out.print("Array 1 holds the following integers: {");
    
    int x = 0;
    int value = 0;
    for (value = 0; value < array1.length; value ++) {
      x = (int)(Math.random()*99);
      array1[value] = x;
      System.out.print (" " + array1[value] + " ");
    }
    System.out.println("}\n");
    
    int count = 0;
    int j = 0;
    for (int i = 0; i < array1.length - 1; ++i) {
      if (array1[j] == i) {
        array2[i]++;
        for (; j <= array1[i]; j++) {
          System.out.println( x + " occurs " + j + " times");
          }
        }
    }
    
    
    
    
    
    /*
    
    
    
    int count = 0;
    int i = 0;
    int j = 0;
    
    for (i = 0; i < array1.length; i++) {                  // numbers from 0 to 99 in the array
        for(j = 0; j < i; j++) {                        // number of occurrences of each integer
          if (array1[j] == i) {
            array2[i]++;
            for (int x = 0; x < array1.length; x++) {
              if (j == 1) {
                System.out.println( x + " occurs " + j + " time");
              }
              else {
                System.out.println( x + " occurs " + j + " times");
              }  
              count++;
            }
            
          }
          
        }
     
    }  

    */
    
   
  } // end of main method
}  // end of public class