//////////////
//// Hanah Roach
//// 9/5/18
//// CSE02 Cyclometer
///
//// My bicycle cyclometer records time (seconds)
//// and number of rotations of front wheel during that time.

public class Cyclometer {
    	// main method required for every Java program
   	public static void main(String[] args) {
      // create variables that will store number of seconds for each trip
      // and the number of counts/rotations for each trip. 
      int secsTrip1=480;  // seconds taken by trip 1
      int secsTrip2=3220;  // seconds taken by trip 2
      int countsTrip1=1561;  // number of rotations by trip 1
      int countsTrip2=9037; // number of rotations by trip 2
      // create variables for useful constants
      // and for storing various distances,
      double wheelDiameter=27.0,  // length of the wheel diameter
      PI=3.14159, // pi value
      feetPerMile=5280,  // how many feet are in a mile
      inchesPerFoot=12,   // how many inches are in a foot
      secondsPerMinute=60;  // how many seconds are in a minute
      double distanceTrip1, distanceTrip2,totalDistance;  // initializing the three as doubles
      // print the stored numbers in the variables that store
      // the number of seconds (converted to minutes) and the counts
      System.out.println("Trip 1 took "+
       	     (secsTrip1/secondsPerMinute)+" minutes and had "+ countsTrip1+" counts."); 
             // prints how many minutes trip 1 took and how many rotations it took.
	    System.out.println("Trip 2 took "+
       	     (secsTrip2/secondsPerMinute)+" minutes and had "+ countsTrip2+" counts.");
      distanceTrip1=countsTrip1*wheelDiameter*PI; // prints how many minutes trip 2 took and how many rotations it took.
    	// Above gives distance in inches
    	//(for each count, a rotation of the wheel travels
    	//the diameter in inches times PI)
      distanceTrip1/=inchesPerFoot*feetPerMile; // Gives distance in miles for trip 1
      distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile; // Gives distance in miles for trip 2
      totalDistance=distanceTrip1+distanceTrip2; // Gives total distance in miles
      //Print out the output data.
      System.out.println("Trip 1 was "+distanceTrip1+" miles"); // Prints the distance of trip 1 in miles
      System.out.println("Trip 2 was "+distanceTrip2+" miles"); // Prints the distance of trip 2 in miles
      System.out.println("The total distance was "+totalDistance+" miles"); // Prints the total distance of trips 1 and 2 in miles
      
	}  //end of main method   
} //end of class
