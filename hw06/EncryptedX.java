//////////////
//// Hannah Roach
//// 10/19/18
//// CSE02 hw 06
///
///
//// Ask user for an integer between 0-100 & validate input...this becomes size of square measured by # of stars.
//// 

import java.util.Scanner;
public class EncryptedX {
  public static void main (String[] args){
    Scanner scan = new Scanner(System.in);

    int input = 0;
    int side = 0;
    boolean validInput = false;
    
    System.out.print("Enter an integer between 0 and 100: ");  // ask the user for an integer for the side length of square
    
    while (!validInput) {
      if (scan.hasNextInt()) {
        input = scan.nextInt();
        if ((input > 0) && (input < 100)) {
          validInput = true;
          side = input;
        }
        else {
          System.out.print("Invalid input; enter an integer between 0 and 100: ");
        }
      }
      else {
        System.out.print("Invalid input; enter an integer between 0 and 100: ");
        scan.next();
       }
    }
    
    
    for (int i = 0; i < side; i++) {
      for (int j = 0; j < side; j++) {
        if ((i == j) || i == (side - (j + 1))) {
          System.out.print(" ");
        }
        else {
          System.out.print("*");
        }
      }
      System.out.println();
    } 
    
  }
}