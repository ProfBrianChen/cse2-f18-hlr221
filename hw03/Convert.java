//////////////
//// Hanah Roach
//// 9/18/18
//// CSE02 Convert
///
//// ask user for doubles that represent the # of acres of land affected by hurricane percipitation
//// ask user for how many inches of rain were dropped on average
//// convert the quantity of rain into cubic miles

import java.util.Scanner;
public class Convert {
  public static void main(String[] args) {
    Scanner myScanner = new Scanner( System.in ) ;
    
    System.out.print("Enter the affected area in acres: ") ;
    
    double acres = myScanner.nextDouble() ;
    
    System.out.print("Enter the inches of rainfall in the affected area: ") ;
    
    double rainfall = myScanner.nextDouble() ; 
    
    double gallonsOfRainfall ;
    double cubicMilesOfRainfall ;
    
    gallonsOfRainfall = (acres * 43560) * (rainfall / 12) ; // convert acres and rainfall to feet
    cubicMilesOfRainfall = gallonsOfRainfall * (0.00000000000679357) ; // convert gallons (cubic feet) to cubic miles
    
    System.out.println(cubicMilesOfRainfall+ " cubic miles") ;
      
  } // end of main class
} // end of method