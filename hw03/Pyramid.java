//////////////
//// Hanah Roach
//// 9/18/18
//// CSE02 Pyramid
///
//// prompt the user for the dimensions of a pyramid
//// return the volume of the pyramid

import java.util.Scanner;
public class Pyramid {
  public static void main(String[] args) {
    Scanner myScanner = new Scanner( System.in ) ;
    
    System.out.print("The square side of the pyramid is (input length): ") ;
    int side = myScanner.nextInt() ;
    
    System.out.print("The height of the pyramid is (input length): ") ;
    int height = myScanner.nextInt() ;
    
    int volume = ((side * side * height) / 3) ;
    System.out.println("The volume inside the pyramid is: "+ volume) ;
    
  } // end of main class
} // end of method