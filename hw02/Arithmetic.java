//////////////
//// Hanah Roach
//// 9/7/18
//// CSE02 Arithmetic
/// 

public class Arithmetic {
  public static void main(String[] args) {
    //Number of pairs of pants
    int numPants = 3;
    //Cost per pair of pants
    double pantsPrice = 34.98;
    
    //Number of sweatshirts
    int numShirts = 2;
    //Cost per shirt
    double shirtPrice = 24.99;
    
    //Number of belts
    int numBelts = 1;
    //cost per belt
    double beltCost = 33.99;
    
    //the tax rate
    double paSalesTax = 0.06;
    
    double totalCostOfPants=((int((((numPants*pantsPrice)*paSalesTax)+(numPants*pantsPrice)))*100) / 100);  //total cost of pants
    double totalCostOfShirts=((int((((numShirts*shirtPrice)*paSalesTax)+((numShirts*shirtPrice)))*100) / 100); //total cost of shirts                      
    double totalCostOfBelts=((int((((numBelts*beltCost)*paSalesTax)+((numBelts*beltCost)*100))) / 100);  //total cost of belts
    // print the total cost of the pants 
    System.out.println("The total cost of pants is "+ (totalCostOfPants)+ " dollars." );
    //print the total cost of the sweatshirts 
    System.out.println("The total cost of sweatshirts is "+ (totalCostOfShirts)+ " dollars.");
    //print the total cost of the belts
    System.out.println("The total cost of belts is "+ (totalCostOfBelts)+ " dollars.");
    
    //print the sales tax charged from buying pants
    System.out.println("The sales tax for buying pants is "+ ((numPants*pantsPrice)*paSalesTax)+ " dollars." );
    //print the sales tax charged from buying sweatshirts
    System.out.println("The sales tax for buying sweatshirts is "+ ((numShirts*shirtPrice)*paSalesTax)+ " dollars.");
    //print the sales tax xharged from buying belts
    System.out.println("The sales tax for buying belts is "+ ((numBelts*beltCost)*paSalesTax)+ " dollars.");
    
    // print the total cost of the pants before tax
    System.out.println("The total cost of pants before tax is "+ (numPants*pantsPrice)+ " dollars." );
    //print the total cost of the sweatshirts before tax
    System.out.println("The total cost of sweatshirts before tax is "+ (numShirts*shirtPrice)+ " dollars.");
    //print the total cost of the belts before tax
    System.out.println("The total cost of belts before tax is "+ (numBelts*beltCost)+ " dollars.");
    
    //print total sales tax
    System.out.println("The total sales tax is "+ (((numPants*pantsPrice)*paSalesTax)+((numShirts*shirtPrice)*paSalesTax)+((numBelts*beltCost)*paSalesTax))+ " dollars.");
    
    //total money paid for the transaction (including sales tax)
    System.out.println("The total amount paid for the transaction is "+ (totalCostOfPants+totalCostOfShirts+totalCostOfBelts)+ " dollars.");
    
    
  }
}