Grading Sheet for HW9

Grade: 70%

Late: RemoveElements late 1 day: -5pts

Two programs each program worth 50 pts

CSE2Linear.java 
Compiles                    5 pts
Comments                 5 pts
Error Checking            10 pts
	-5pts: didn’t check for string, incorrect check for out of range, incorrect check for ascending order
Linear search method            10 pts
	-5pts: didn’t indicate that grade wasn’t found
Binary Search method        10 pts
Shuffle method            10 pts
	-5pts: incorrectly shuffled


    
RemoveElements.java
Compiles                    5 pts
Comments                 5 pts
Randominput method        10 pts
delete method            10 pts
	-5pts: no error check for out of range positions
Remove method             10 pts
	-5pts: a random 0 showed up at the end of your array
Works with provided main method      10  pts