////
/// Hannah Roach
/// 11/25/18
/// CSE 002 hw09
////
/// Program 1:
/// prompts the user to enter 15 ints for students' final grades 
/// check that the user enters only ints 
/// print an error message if:
///// not an int
///// not within 0-100
///// the int is not greater than the last
/// print final input array
/// prompt user to enter a grade to be searched for and use BINARY search to find the grade
/// indicate if grade was found or not, and print number of iterations used
// then
/// scramble the array and print it.
/// prompt user to enter an int to be searched for and use LINEAR search to find the grade
/// indicate if the grade was found or not and how many iterations it took
///// remember: write separate methods for linear search, binary search, and scrambling.

import java.util.Scanner;
import java.util.Random;
import java.util.Arrays;
public class CSE2Linear { //will change once in Code Anywhere
  public static void main (String[]args) {
    Scanner scan = new Scanner(System.in);
    System.out.println("Enter 15 ascending integers for students' final grades in CSE2:");
    int grades[] = new int[15];
    int grade = 0;
    
    for (int i = 1; i < grades.length; i++) {
      
      Boolean validInput = false;
      
      while (!validInput) {
      
        grade = scan.nextInt();
        if (grade < grades[i - 1]) {
          System.out.print("Not ascending order. Enter a higher grade than the last: ");
          scan.next();
        }
        else {
          if (grade < 0 && grade > 100) {
            System.out.print("Out of bounds. enter a grade within 0-100: ");
            scan.next();
          }
          else {
            if (!scan.hasNextInt()) {
              System.out.print("Not an integer. Please enter an integer: ");
              scan.next();
            }
            else {
              validInput = true;
            }
          }
        }    
      } // end of while loop for valid input
      
       grades[i] = grade; 
       
      
    } // end of for loop for values in grade array
    
    
    System.out.print(Arrays.toString(grades) + " ");
    
    
    System.out.println("Enter a grade to be searched for: ");
    int num = scan.nextInt();
    System.out.println(num);
    binarySearch(grades, num);
    
    System.out.println("Scrambled: ");
    scrambling(grades);
    System.out.println();
    
    System.out.print("Enter a grade to be searched for: ");
    int num2 = scan.nextInt();
    linearSearch(grades, num2);
    //int iteration2;

    
  } // end of main method
  
  
  
  
  
  public static void binarySearch(int[] grades, int num) {
    
    int low = 0;
    int high = grades.length - 1;
    int iteration = 0;
    String found = "not";
    
    while (high >= low) {
      
      iteration++;
      
      int mid = (low + high) / 2;
      
      if (num < grades[mid]) {
        high = mid - 1;
      }
      else if (num == grades[mid] / 2) {
        found = "";
        System.out.println(mid + " was " + found + " found with " + iteration + " iterations.");
       
      }
      else {
        low = mid + 1;
      }
     
      
    } // end of while loop
      
  } // end of binarySearch() method
  
  
  
  
  
  public static void scrambling(int[] grades) {
    
    Random random = new Random(); // generated a random number
    
    for (int i = 0; i < grades.length; i++) {
      int randPos = random.nextInt(grades.length);
      int temp = grades[i];
      grades[i] = grades[randPos];
      grades[randPos] = temp;
      System.out.print(temp + " ");
    } // end of for loop
    System.out.println();

    
  } // end of scrambling() method
  
  
  
  
  
  public static int linearSearch(int[] grades, int num2) {
    
    String found = "not";
    for (int i = 0; i < grades.length; i++) {
      if (num2 == grades[i]) {
        found = "";
        System.out.println(num2 + " was " + found + " found with " + i + " iterations.");
        return i;
      }
    } // end of for loop
    return -1;
    
  } // end of linearSearch() method
  
  
  
  
} // end of public class