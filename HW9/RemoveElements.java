////
/// Hannah Roach
/// 11/25/18
/// CSE 002 hw09
////
/// Program 2:
/// randomInput()
/////  generates an array of 10 random integers from 0 to 9
/////  it should fill the array with random ints and returns the filled array
/// delete(list,pos) takes an int array called list and an int called pos as inputs
/////  creates a new array that has one member fewer than list 
/////  it should be composed of all the same members except the one in position pos
/// remove(list,target) 
/////  deletes all the elements that are equal to the target (an integer)
/////  returns a new list without the target elements

import java.util.Scanner;
import java.util.Random;
public class RemoveElements{
  public static void main(String [] arg){
    Scanner scan=new Scanner(System.in);
    int num[]=new int[10];
    int newArray1[];
    int newArray2[];
    int index,target;
    String answer="";
    do{
      System.out.print("Random input 10 ints [0-9]");
      num = randomInput();
      String out = "The original array is:";
      out += listArray(num);
      System.out.println(out);
      
      System.out.print("Enter the index ");
      index = scan.nextInt();
      newArray1 = delete(num,index);
      String out1="The output array is ";
      out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
      System.out.println(out1);
      
      System.out.print("Enter the target value ");
      target = scan.nextInt();
      newArray2 = remove(num,target);
      String out2="The output array is ";
      out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
      System.out.println(out2);
      
      System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
      answer=scan.next();
    }while(answer.equals("Y") || answer.equals("y"));
  } // end of main method
  
  public static String listArray(int num[]){
    String out="{";
    for(int j=0;j<num.length;j++){
      if(j>0){
        out+=", ";
      }
      out+=num[j];
    }
    out+="} ";
    return out;
  } // end string method
  
  
  
  
  
  public static int[] randomInput() {
    
    int[] list = new int[10];
    for (int i = 0; i < 10; i++) {
      list[i] = (int)(list.length * Math.random());
    }
    return list;
    

    
    
  } // end of random input method
  

  
  public static int[] delete(int[] list, int pos) {
    int[] newArray = new int [list.length - 1];
    int j = 0;
    for (int i = 0; i <= list.length - 1; i++) {
      if (i != pos) {
        newArray[j] = list[i];
        j++;
      }
     }
    return newArray;
    
  } // end of delete method
  

  
  
  public static int[] remove(int[] list, int target) {
    
    int n = 0;
    for (int i = 0; i <= list.length - 1; i++) {
      if (list[i] == target) {
        n++;
      }
    }
    int[] newArray = new int [10 - n];
    
    int j = 0;
    for (int i = 0; i < list.length - 1; i++) {
      if (list[i] != target) {
        newArray[j] = list[i];
        j++;
      }
    }
    return newArray;
    
  }
  

  
} // end of public class