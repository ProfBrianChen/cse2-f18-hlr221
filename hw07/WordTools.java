//////////////
//// Hannah Roach
//// 10/28/18
//// CSE02 Hw07 Word Tools
///
//// Use a sampleText() method asking the user to enter a string of their choosing
//// Then use a printMenu() method to output a menu of user options for analyzing/editing the string
//// if an invalid character is entered, continue to prompt for a valid choice. Call printMenu() in the main() method
//// in main menu, contribute to call printMenu() each time the user enters an incorrect input until user enters "q" to quit
//// call the corresponding method after user enters accpetable inputs
//// Then use getNumOfNonWSCharacters() method (string parameter, returns # of characters in string (excluding whitespaces))
//// call if user enters "c" in the menu
//// Then use getNumOfWords() (string parameter, returns # of words in the string)
//// call if user enters "w" in menu
//// Then use findText() (2 string parameters...1 in text and 1 is user provided; returns # of times word is found in text)
//// call if user enters "f" in menu
//// Then use replaceExclamation() (string parameter, replaces each "!" with a ".")
//// call if user enters "r",  outputs the edited string.
//// Then use shortenSpace() (string parameter, returns a string that replaces all sequences of 2+ spaces w/ 1 space).
//// call if user enters "s", outputs edited string.


import java.util.Scanner;
import java.lang.Character;

public class WordTools {
  public static void main(String[]args) {
    
    
    Scanner scanner = new Scanner(System.in);

    String text = text();
    boolean quitProgram = false;
    char choice = ' ';
    
    while (!quitProgram) {
      choice = printMenu();
      if (choice == 'q') {    // requests to quit program.
        quitProgram = true;
      }
      if (choice != 'q') {    // continues to run the program.
        switch (choice) {
          case 'c': 
            getNumOfNonWSCharacters(text); 
            break;
          case 'w': 
            getNumOfWords(text); 
            break;
          case 'f': 
            findText(text); 
            break;
          case 'r': 
            replaceExclamation(text); 
            break;
          case 's': 
            shortenSpace(text); 
            break;
          default: 
            System.out.print("\nInvalid choice, choose one of the given letters. \n");
            break;
        }
      }
    }
    
    
  }     // end of main method
  
  
  
  
//////////////////////////////////////////////  text method  //////////////////////////////////////////////////
  public static String text() { 
    
    
    Scanner scanner = new Scanner(System.in);
    
    System.out.println("Enter a sample text: ");    // the user inputs a sample text.
    String text = scanner.nextLine();
    
    System.out.println("You entered: " + text);
    return text;
    
    
  }     // end of text method
  
  
  
  
//////////////////////////////////////////////  menu method  //////////////////////////////////////////////////  
  public static char printMenu() {
    
    
    Scanner scanner = new Scanner(System.in);
    
    System.out.println("\nMENU \nc - Number of non-whitespace characters \nw - Number of words");
    System.out.println("f - Find text \nr - Replace all !'s \ns - Shorten spaces \nq - Quit\n");
    
    return scanner.next().charAt(0);    // returns a chance for the user to input a char.
    
    
  }    // end of print menu method
    
  
  
  
//////////////////////////////////////////  # non-wspace method  //////////////////////////////////////////////// 
  public static int getNumOfNonWSCharacters(String text) {   
    
    int spaces = 0;
    int characters = 0;
    for(int i = 0; i <= (text.length() - 1); i++) {
        if (text.charAt(i) == ' ') {    // finds spaces
          spaces++;
        }
        else {
          characters++;
        }
      } 
    System.out.println("\nNumber of non-whitespace characters: " + characters);
    return characters;    // returns the number of non-ws characters within the sample text.
    
       
  }     // end of num of non-whitespace characters method 
  
  
  
  
  
////////////////////////////////////////////  # words method  ////////////////////////////////////////////////// 
  public static int getNumOfWords(String text){ 

    int words = 1;
    for(int i = 0; i < (text.length()); i++) {
      if(text.charAt(i) == ' ') {   // finds words
        words++;
      }
    }
    System.out.println("\nNumber of words: " + words);  
    return words;    // returns the number of words within the sample text.
        
    
  }     // end of num of words method
  
  
  
  
  
////////////////////////////////////////  find text method  //////////////////////////////////////////////////
  public static int findText(String text) {
    
    Scanner scanner = new Scanner(System.in);
    System.out.println("Enter a word or phrase to be found: ");
    String input = scanner.nextLine();
    
    int instances = 0;
    while (text.contains(input)) {
      if (text.contains(input)) {
        text = text.replaceFirst(input, "");
        instances++;    // increments the amount of times the word/phrase appears in the text
      }
    }
    System.out.println("\n" + input + " instances: " + instances);
    return instances;   // returns the number of instances of the word/phrase.
    
    
   }     // end of find text method 
    
  
  
  
///////////////////////////////////  replace exclamations method  //////////////////////////////////////////////  
  public static String replaceExclamation(String text) {
    
    char period = '.';
    char ex = '!';
    for(int i = 0; i < text.length() - 1; i++) {
      text = text.replace(ex, period);    // replaces the exclamation mark with a period each time one is found
    }
    System.out.println("\nEdited text: " + text);
    return text;    // returns the new, edited text.
    

  }     // end of replace exclamations method
  
  
    
  
/////////////////////////////////////////  shorten spaces method  ///////////////////////////////////////////////
  public static String shortenSpace(String text) {
    
    String twoSpace = "  ";
    String oneSpace = " ";
    for(int i = 0; i < text.length() - 1; i++) {
      text = text.replace(twoSpace, oneSpace);   // replaces a double space with a single space each time one is found
    }
    System.out.println("\nEdited text: " + text);
    return text;    // returns the new, edited text.
    
    
  }     // end of shorten spaces method
  
  
  
  
/////////////////////////////////////////  end of program!  ///////////////////////////////////////////////
  
  
  
}  // end of public class Word Tools