//////////////
//// Hanah Roach
//// 9/21/18
//// CSE02 Craps Switch
///
//// Use only switch statements
//// Ask the user if they'd like to randomly cast dice or if they'd like to state the two dice they want to evaluate
//// If they want to randomly cast dice. generate two random numbers from 1-6, inclusive, representing the outcomes of the two dice cast in the game
//// If they want user provided dice, ise the Scanner twice to ask the user for two intergers from 1-6, incluseve. Assume the user always provides an interger, but check if the interger is within range.
//// Determine the slang terminology of the outcome of the roll
//// print out the slang terminology

import java.util.Random;
import java.util.Scanner;
import java.lang.Math;
public class CrapsSwitch {
  public static void main(String[] args) {
    
    int dice1 = 0;
    int dice2 = 0;
    int scoreForEqualDiceValues = 0;
    int score = 0;
    int diceNum = 0;
    
    Scanner myScanner = new Scanner(System.in);
    
    // choose whether to roll randomly or pick number 
    System.out.println("Type 1 to roll randomly.");
    System.out.println("Type 2 to pick your own dice values.");
    
            ///check if interger is within range!!! say something if there is an error. !!!!
    
    int answer = myScanner.nextInt();
         
    switch(answer){        // use two cases, the first one for a case chosen randomly, the second for picking your own dice values.
      case 1:
       dice1 = (int)(Math.random() * 6 + 1);
       dice2 = (int)(Math.random() * 6 + 1);
       score = dice1 + dice2;
       diceNum = Math.abs(dice1 - dice2); // to later determine whether the slang terminology uses "hard" or "easy" for equal score values.
        // based on the score and combination of the chosen dice, print the slang terminology for the values from the dice.

        
        switch(score){
          case 2:
            System.out.println("Snake Eyes");
            break;
            
          case 3:
            System.out.println("Ace Deuce");
            break;
            
          case 4:
            switch(diceNum){
              case 0:
                System.out.println("Hard Four");
                break;
              case 2:
                System.out.println("Easy Four");
            }   
            break;
            
          case 5:
            System.out.println("Fever Five");
            break;
            
          case 6:
            switch(diceNum){
              case 0:
                System.out.println("Hard Six");
                break;
              case 2:
                System.out.println("Easy Six");
                break;
              case 4:
                System.out.println("Easy Six");
                break;
            }   
            break;
            
          case 7:
            System.out.println("Seven Out");
            break;
            
          case 8:
            switch(diceNum){
              case 0:
                System.out.println("Hard Eight");
                break;
              case 2:
                System.out.println("Easy Eight");
                break;
            }   
            break;
            
          case 9:
            System.out.println("Nine");
            break;
            
          case 10:
            switch(diceNum){
              case 0:
                System.out.println("Hard Ten");
                break;
              case 2:
                System.out.println("Easy Ten");
                break;
            }   
            break;
            
          case 11:
            System.out.println("Yo-leven");
            break;
            
          case 12:
            System.out.println("Boxcars");
            break;
        }

    break; // end of case 1, so that the code stops here if you chose to roll randomly.
        
     //this is now the case if you choose the dice value        
      case 2: 
       System.out.print("Enter a number between 1 and 6, inclusive, for dice 1: ");
       dice1 = (int)myScanner.nextInt();
       System.out.print("Enter a number between 1 and 6, inclusive, for dice 2: ");
       dice2 = (int)myScanner.nextInt(); 
               
       score = dice1 + dice2;
       diceNum = Math.abs(dice1 - dice2); // to later determine whether the slang terminology uses "hard" or "easy" for equal score values.
        // based on the score and combination of the chosen dice, print the slang terminology for the values from the dice.
        switch(score){
          case 2:
            System.out.println("Snake Eyes");
            break;
            
          case 3:
            System.out.println("Ace Deuce");
            break;
            
          case 4:
            switch(diceNum){
              case 0:
                System.out.println("Hard Four");
                break;
              case 1:
                System.out.println("Easy Four");
            }   
            break;
            
          case 5:
            System.out.println("Fever Five");
            break;
            
          case 6:
            switch(diceNum){
              case 0:
                System.out.println("Hard Six");
                break;
              case 2:
                System.out.println("Easy Six");
                break;
              case 4:
                System.out.println("Easy Six");
                break;
            }   
            break;
            
          case 7:
            System.out.println("Seven Out");
            break;
            
          case 8:
            switch(diceNum){
              case 0:
                System.out.println("Hard Eight");
                break;
              case 2:
                System.out.println("Easy Eight");
                break;
            }   
            break;
            
          case 9:
            System.out.println("Nine");
            break;
            
          case 10:
            switch(diceNum){
              case 0:
                System.out.println("Hard Ten");
                break;
              case 2:
                System.out.println("Easy Ten");
                break;
            }   
            break;
            
          case 11:
            System.out.println("Yo-leven");
            break;
            
          case 12:
            System.out.println("Boxcars");
            break;
        }

    }
    
  
  } // end of public class
} // end of main method
