// Hannah Roach
// 12/4/18
// CSE 2 HW10
///// 
///// create a tic tac toe game using 2D arrays and multiple methods.
///// have the code print the board after every move.


import java.util.Scanner;
import java.util.Arrays;
public class HW10 {
  public static void main(String[] args) {
    
    char[][] board = createBoard();
    
    boolean noMoreMoves = false;
    
     // doesn't print anything
      
    int pos = 0;
    
    while (pos < 9) {
      
      if (pos % 2 != 0) {
        pos = check(); 
        x(pos, board);
      }
      else { 
        pos = check(); 
        o(pos, board);
      }
      System.out.println();
      System.out.println(replacePosWithX(board, pos));
      System.out.print("Enter another position: ");
      System.out.println(replacePosWithX(board, pos));
      checkWinner(board, pos);
      pos++;
    }
    
  } // end of main method
  
  
  
  
  public static char[][] createBoard() {
    
    char[][] board = new char[3][3];
    
    //char count = 1;
    board[0][0] = '1';
    board[0][1] = '2';
    board[0][2] = '3';
    board[1][0] = '4';
    board[1][1] = '5';
    board[1][2] = '6';
    board[2][0] = '7';
    board[2][1] = '8';
    board[2][2] = '9';
        
    System.out.println('1'+"   "+'2'+"   "+'3'); 
    System.out.println('4'+"   "+'5'+"   "+'6'); 
    System.out.println('7'+"   "+'8'+"   "+'9'); 
    return board;
    
  } // end of create board method
  
  
  
  public static int check() {
    
    Scanner scan = new Scanner(System.in);
    int pos = 0;
    boolean validInput = false;
    
    System.out.print("Enter your position: "); 
    
    while (!validInput) {
      if (scan.hasNextInt()) {
        pos = scan.nextInt();
        if ((pos > 0) && (pos < 10)) {
          validInput = true;
        }
        else {
          System.out.print("Invalid input: ");
        }
      }
      else {
        System.out.print("Invalid input: ");
        scan.next();
       }
    }
    return pos;
  } // end of check method
  
  
  
  
  
  public static char[][] x(int pos, char[][] board) {
    
    for(int i = 0; i < board.length; i++)
    {
      for(int j = 0; j < board[0].length; j++)
      {
        if (pos == board[i][j])
        {
          board[i][j] = 'x';
        }
      }
    }
    return board;
  } // end of x method
  
  
  
  
      
  public static char[][] o(int pos, char[][] board) {
    
    for(int i = 0; i < board.length; i++)
    {
      for(int j = 0; j < board[0].length; j++)
      {
        if (pos == board[i][j])
        {
          board[i][j] = 'o';
        }
      }
    }
    return board;
  } // end of o method
  
  
    
  public static void printBoard (char[][] board) {
    for(int i = 0; i < 3; i++)
    {
      for(int j = 0; j < 3; j++)
      {
        System.out.println(board[i][j] + " ");
      }
    } 
      
  } // end of print board method
  
  
  
  
  
  public static char[][] replacePosWithX(char[][] board, int pos) {
    
    
    // String line1 = board[0][0] + board[1][0] + board[2][0]; or something like this to separate the lines
    char charPos = (char) pos;
    switch (charPos) {
      case '1':
        // return 'x';
        board[0][0] = 'x';
        break;
      case '2':
        board[0][1] = 'x'; 
        break;
      case '3':
        board[0][2] = 'x';
        break;
      case '4': 
        board[1][0] = 'x';
        break;
      case '5':
        board[1][1] = 'x';
        break;
      case '6':
        board[1][2] = 'x';
        break;
      case '7':
        board[2][0] = 'x';
        break;
      case '8':
        board[2][1] = 'x';
        break;
      case '9':
        board[2][2] = 'x';
        break;
      default:
        break;
    }
    
    
    
   /* 
    if (pos = '1') {
      System.out.println('x'+"   "+'2'+"   "+'3'); 
      System.out.println('4'+"   "+'5'+"   "+'6'); 
      System.out.println('7'+"   "+'8'+"   "+'9'); 
    }
    else if (pos = '2') {
      System.out.println('1'+"   "+'x'+"   "+'3'); 
      System.out.println('4'+"   "+'5'+"   "+'6'); 
      System.out.println('7'+"   "+'8'+"   "+'9'); 
    }
    else if (pos = '3') {
      System.out.println('1'+"   "+'2'+"   "+'x'); 
      System.out.println('4'+"   "+'5'+"   "+'6'); 
      System.out.println('7'+"   "+'8'+"   "+'9'); 
    }
    else if (pos = '4') {
      System.out.println('1'+"   "+'2'+"   "+'3'); 
      System.out.println('x'+"   "+'5'+"   "+'6'); 
      System.out.println('7'+"   "+'8'+"   "+'9'); 
    }
    else if (pos = '5') {
      System.out.println('1'+"   "+'2'+"   "+'3'); 
      System.out.println('4'+"   "+'x'+"   "+'6'); 
      System.out.println('7'+"   "+'8'+"   "+'9'); 
    }
    else if (pos = '6') {
      System.out.println('1'+"   "+'2'+"   "+'3'); 
      System.out.println('4'+"   "+'5'+"   "+'x'); 
      System.out.println('7'+"   "+'8'+"   "+'9'); 
    }
    else if (pos = '7') {
      System.out.println('1'+"   "+'2'+"   "+'3'); 
      System.out.println('4'+"   "+'5'+"   "+'6'); 
      System.out.println('x'+"   "+'8'+"   "+'9'); 
    }
    else if (pos = '8') {
      System.out.println('1'+"   "+'2'+"   "+'3'); 
      System.out.println('4'+"   "+'5'+"   "+'6'); 
      System.out.println('7'+"   "+'x'+"   "+'9'); 
    }
    else if (pos = '9') {
      System.out.println('1'+"   "+'2'+"   "+'3'); 
      System.out.println('4'+"   "+'5'+"   "+'6'); 
      System.out.println('7'+"   "+'8'+"   "+'x'); 
    }
*/
    
    return board;
      //(board[0][0] + board[0][1] + board[0][2] + board[1][0] + board[1][1] + board[1][2] + board[2][0] + board[2][1] + board[2][2]);  // says " incompatible types: int cannot be converted to char[][]"
  } // end of replace pos with x method
  
  
  
  
  
  
  public static char[][] replacePosWithO(char[][] board, int pos) {
    
    // ?????????? will be basically the same as x
    
    return board; 
  } // end of replace pos with o method
  
  
  
  
  
  public static String checkWinner(char[][] board, int pos) {
    
    // for x to win
    if (board[0][0]=='x' && board[0][1]=='x' && board[0][2]=='x') {
      return "x wins";
    }
    else if (board[1][0]=='x' && board[1][1]=='x' && board[1][2]=='x') {
      return "x wins";
    }
    else if (board[2][0]=='x' && board[2][1]=='x' && board[2][2]=='x') {
      return "x wins";
    }
    else if (board[0][0]=='x' && board[1][0]=='x' && board[2][0]=='x') {
      return "x wins";
    }
    else if (board[0][1]=='x' && board[1][1]=='x' && board[2][1]=='x') {
      return "x wins";
    }
    else if (board[0][2]=='x' && board[1][2]=='x' && board[2][2]=='x') {
      return "x wins";
    }
    else if (board[0][0]=='x' && board[1][1]=='x' && board[2][2]=='x') {
      return "x wins";
    }
    else if (board[0][2]=='x' && board[1][1]=='x' && board[2][0]=='x') {
      return "x wins";
    }
    
    
    // for o to win
    if (board[0][0]=='o' && board[0][1]=='o' && board[0][2]=='o') {
      return "o wins";
    }
    else if (board[1][0]=='o' && board[1][1]=='o' && board[1][2]=='o') {
      return "o wins";
    }
    else if (board[2][0]=='o' && board[2][1]=='o' && board[2][2]=='o') {
      return "o wins";
    }
    else if (board[0][0]=='o' && board[1][0]=='o' && board[2][0]=='o') {
      return "o wins";
    }
    else if (board[0][1]=='o' && board[1][1]=='o' && board[2][1]=='o') {
      return "o wins";
    }
    else if (board[0][2]=='o' && board[1][2]=='o' && board[2][2]=='o') {
      return "o wins";
    }
    else if (board[0][0]=='o' && board[1][1]=='o' && board[2][2]=='o') {
      return "o wins";
    }
    else if (board[0][2]=='o' && board[1][1]=='o' && board[2][0]=='o') {
      return "o wins";
    }
    
    else {
      return "draw";
    }
    
  } // end of check winner method
    
    /*
    char charPos = (char) pos;
    char[] line = '';
    for (int i = 0; i < 3; i++) {
      for (int j = 0; j < 3; j++) {
        //System.out.println(board[i][j]);
        switch (pos) {
          case 0:
            line = board[0] + board[1] + board[2];
            break;
          case 1:
            line = board[3] + board[4] + board[5];
            break;
          case 2:
            line = board[6] + board[7] + board[8];
            break;
          case 3:
            line = board[0] + board[3] + board[6];
            break;
          case 4:
            line = board[1] + board[4] + board[7];
            break;
          case 5:
            line = board[2] + board[5] + board[8];
            break;
          case 6:
            line = board[0] + board[4] + board[8];
            break;
          case 7:
            line = board[2] + board[4] + board[6];
            break;
      }
    }
    
  } // end of check winnner method
  
  */

  // disregard these comments
  //for(int i = 0; i < 3; i++)
  //{
    //if board[i] == x; 
    //increment count 
  // for loop for row
  //if x 
  //count var ++ 
  //if o
  //count var ++

  
} // end of public class
