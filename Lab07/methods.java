//////////////
//// Hannah Roach
//// 10/25/18
//// CSE02 Lab07 Methods
///
//// Create four methods that correspond to the four components:
//// 1) Adjectives
//// 2) Non-primary nouns (for a subject)
//// 3) Past-tense verbs
//// 4) Non-primary nouns (for an object)
//// Make each method generate a random integer from 0-9
//// select a random adjective/subject/verb/object from a switch statement
//// return the words as a string

import java.util.Scanner;
import java.util.Random;
public class methods {  
// main method with loop to assemble and print the words into a sentence
// create a loop that, in each iteration, assembles the words into a String and prints a sentence
// make sure articles between random words should be appropriate
  public static void main(String[] args) {
    
    Scanner scanner = new Scanner (System.in);
    
    Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(10);
    
    
    String adjective = "a";
    String subject = "b";
    String verb = "c";
    String object = "d";
    
    String adjective1 = adjective(adjective);
    String subject1 = subject(subject);
    String verb1 = verb(verb);
    String object1 = object(object); 
    
    String adjective2 = adjective(adjective);
    String subject2 = subject(subject);
    String verb2 = verb(verb);
    String object2 = object(object); 
    
    String adjective3 = adjective(adjective);
    String subject3 = subject(subject);
    String verb3 = verb(verb);
    String object3 = object(object); 
    
    String adjective4 = adjective(adjective);
    String subject4 = subject(subject);
    String verb4 = verb(verb);
    String object4 = object(object); 
    
    String adjective5 = adjective(adjective);
   
    System.out.println("The " + adjective1 + " " + adjective2 + " " + subject1 + " " + verb1 + " the " + adjective3 + " " + object1 + ".");
    
    System.out.print("Would you like another sentence? Enter 1 for yes and anything else for no. "); // ask the user if they would like another sentence
    int input = scanner.nextInt();
    boolean validInput = false;
    while (!validInput) {
       if (input == 1) {
         input = scanner.nextInt();
         validInput = true;
       }
       else {
         break;
       }
    }
    
    System.out.println("This " + subject1 + " was " + adjective3 + " " + verb2 + " to " + adjective4 + " " + object3 + ".");
    System.out.println("It used " + object1 + " to " + verb3 + " " + subject2 + " at the " + adjective5 + " " + object4 + ".");
    
    System.out.println("Paragraph:");
    System.out.print("The " + adjective1 + " " + adjective2 + " " + subject1 + " " + verb1 + " the " + adjective3 + " " + object1 + ". ");
    System.out.print("This " + subject1 + " was " + adjective3 + " " + verb2 + " to " + adjective4 + " " + object3 + ". ");
    System.out.print("It used " + object1 + " to " + verb3 + " " + subject2 + " at the " + adjective5 + " " + object4 + ". ");
    System.out.print("That " + subject1 + " " + verb4 + " her " + subject3 + "!");
    
    
    //for even more methods option:
    //System.out.print(sentenceTwo(subject1, object1, verb1, subject2, adjective1, object2) + " " + sentenceThree(subject1, object1, verb1, subject2, adjective1, object2) +  " " + conclusion(subject1, verb1, object1));

    
    
  }
                                // end of main method

  
  
  
  
/////////////////////////////////////////////////////////////////////////


// Method to create adjectives
  public static String adjective (String adjective) {
    Random randomGenerator = new Random ();
    int randomInt = randomGenerator.nextInt(10);
    //String adjective;
    switch (randomInt) {
      case 0: adjective = "ravenous"; break;
      case 1: adjective = "amiable"; break;
      case 2: adjective = "vile"; break;
      case 3: adjective = "compassionate"; break;
      case 4: adjective = "heuristic"; break;
      case 5: adjective = "impish"; break;
      case 6: adjective = "wise"; break;
      case 7: adjective = "tenacious"; break;
      case 8: adjective = "pompous"; break;
      case 9: adjective = "silly"; break;
      default: break;
    }
    return adjective; 
    //System.out.print(adjective);
  }
                                  // end of adjective method
  
// Method to create a subject
  public static String subject (String subject) {
    Random randomGenerator = new Random ();
    int randomInt = randomGenerator.nextInt(10);
    //String subject;
    switch (randomInt) {
      case 0: subject = "honey"; break;
      case 1: subject = "boat"; break;
      case 2: subject = "doctor"; break;
      case 3: subject = "angel"; break;
      case 4: subject = "boot"; break;
      case 5: subject = "country"; break;
      case 6: subject = "creator"; break;
      case 7: subject = "jewel"; break;
      case 8: subject = "vegetation"; break;
      case 9: subject = "voyage"; break;
      default: break;
    }
    return subject;   
    //System.out.print(subject);
  }
                                  // end of subject method
  
// Method to create a verb
  public static String verb (String verb) {
    Random randomGenerator = new Random ();
    int randomInt = randomGenerator.nextInt(10);
    //String verb;
    switch (randomInt) {
      case 0: verb = "gravitated to"; break;
      case 1: verb = "shattered"; break;
      case 2: verb = "hobbled"; break;
      case 3: verb = "illuminated"; break;
      case 4: verb = "smited"; break;
      case 5: verb = "demolished"; break;
      case 6: verb = "downloaded"; break;
      case 7: verb = "engulfed"; break;
      case 8: verb = "untangled"; break;
      case 9: verb = "revolutionized"; break;
      default: break;
    }
    return verb;  
    //System.out.print(verb);
  }
                                  // end of verb method
  
// Method to create an object
  public static String object (String object) {
    Random randomGenerator = new Random ();
    int randomInt = randomGenerator.nextInt(10);
    //String object;
    switch (randomInt) {
      case 0: object = "animal"; break;
      case 1: object = "house"; break;
      case 2: object = "toothpaste"; break;
      case 3: object = "river"; break;
      case 4: object = "ball"; break;
      case 5: object = "muscle"; break;
      case 6: object = "fireman"; break;
      case 7: object = "ministry"; break;
      case 8: object = "froggy"; break;
      case 9: object = "quicksand"; break;
      default: break;
    }
    return object;   
    //System.out.print(object);
  }
                                  // end of object method
  
  /**
  public static String sentenceOne (String adjective1, String adjective2, String subject1, String verb1, String adjective3, String object1) {
    System.out.print("The " + adjective1 + " " + adjective2 + " " + subject1 + " " + verb1 + " the " + adjective3 + " " + object1 + ".");
    return "a";
    //System.out.println(a);
    //return a; 
  }
  
                                  // end of sentenceOne method
  
  
  public static String sentenceTwo (String subject1, String adjective3, String verb2, String adjective4, String object2) {
    System.out.print("This " + subject1 + " was " + adjective3 + " " + verb2 + " to " + adjective4 + " " + object3 + ".");
    //System.out.println(b);
    return "b"; 
  }
  
                                  // end of sentenceTwo method
  
  
    public static String sentenceThree (String verb3, String subject2, String adjective5, String object4) {
    System.out.print("It used " + object1 + " to " + verb3 + " " + subject2 + " at the " + adjective5 + " " + object4 + ".");
    //System.out.println(c);
    return "c"; 
  }
  
                                   // end of sentenceThree method
  
        
    public static String conclusion (String subject1, String verb4, String subject3) {
    System.out.print("That " + subject1 + " " + verb4 + " her " + subject3 + "!");
    //System.out.println(d);
    return "d";  
  }
  
                                   // end of conclusion method
  
  */
  
}

