//////////////
//// Hannah Roach
//// 10/11/18
//// CSE02 Lab06 Pattern D
///
///
//// Ask the user for an integer between 1 - 10 for the length/number of rows for the pyramid.
//// Check that the inputs are between 1 and 10, and make sure user inputs an integer/an integer in range (indicate error & ask again)
//// Print out Pyramid D
//// 
import java.util.Scanner;
public class PatternD {
  public static void main (String[] args){
    Scanner scan = new Scanner(System.in);

    int number = 0;
    int input = 0;
    boolean validInput = false;
    
    System.out.print("Enter an integer between 1 and 10: ");  // ask the user for an integer for the length of the pyramid
    
    while (!validInput) {
      if (scan.hasNextInt()) {
        input = scan.nextInt();
        if ((input > 0) && (input < 11)) {
          validInput = true;
          number = input;
        }
        else {
          System.out.print("Invalid input; enter an integer between 1 and 10: ");
        }
      }
      else {
        System.out.print("Invalid input; enter an integer between 1 and 10: ");
        scan.next();
       }
    }
    
    int rows = number;
    int nums = number;
    while (rows >= 1) {
      nums = rows;
      while (nums >= 1) {
        System.out.print(nums + " ");
        nums--;
      }
      System.out.println();
      rows--;
      }
    }
  }