//////////////
//// Hannah Roach
//// 10/11/18
//// CSE02 Lab06 Pattern A
///
///
//// Ask the user for an integer between 1 - 10 for the length/number of rows for the pyramid.
//// Check that the inputs are between 1 and 10, and make sure user inputs an integer/an integer in range (indicate error & ask again)
//// Print out PyramidA
//// 
import java.util.Scanner;
public class PatternA {
  public static void main (String[] args){
    Scanner scan = new Scanner(System.in);

    int number = 0;
    int input = 0;
    boolean validInput = false;
    
    System.out.print("Enter an integer between 1 and 10: ");  // ask the user for an integer for the length of the pyramid
    
    while (!validInput) {
      if (scan.hasNextInt()) {
        input = scan.nextInt();
        if ((input > 0) && (input < 11)) {
          validInput = true;
          number = input;
        }
        else {
          System.out.print("Invalid input; enter an integer between 1 and 10: ");
        }
      }
      else {
        System.out.print("Invalid input; enter an integer between 1 and 10: ");
        scan.next();
       }
    }
    
      int rows = 1;
      int nums = 1;
      while (rows <= input) {
        while (nums <= rows) {
          System.out.print(nums + " ");
          nums++;
       }
       nums = 1;
       System.out.println();
       rows++;
      }
    }
  }
