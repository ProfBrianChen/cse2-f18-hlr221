//////////////
//// Hanah Roach
//// 9/20/18
//// CSE02 Card Generator
///
//// Pick a random card from the "deck" using a random number generator [1-52]. 
//// 1-13 are diamonds, 14-26 clubs, 27-39 hearts, 40-52 spades. All suits ascend from ace until king. 

import java.lang.Math;
import java.util.Scanner;
import java.util.Random;
public class CardGenerator {
  public static void main(String[] args) {
    Scanner myScanner = new Scanner( System.in );
       
    // identify the terms "suit" and "card"
    String suit = "a";
    String card = "b";
   
    int cardNumber = (int)(Math.random() * 52 + 1); // generate random number from 1 to 52
    
    // group the card into suits
    if (cardNumber <=13) {
      suit = "diamonds";
    }
    else if ( (cardNumber >= 14) && (cardNumber <= 26) ) {
      suit = "clubs";
    }
    else if ( (cardNumber >= 27) && (cardNumber <= 39) ) {
        suit = "hearts";
    }
    else if ( (cardNumber >= 40) && (cardNumber <= 52) ) {
        suit = "spades";
    }
    int remainder = (int)cardNumber % 13;
    
    // identify card values and names
    switch(remainder){
      case 0:
        card = "King";
      case 1:
        card = "Queen";
        break; 
      case 2:
        card = "Jack";
        break; 
      case 3: 
        card = "9";
        break;
      case 4:
        card = "8";
        break;
      case 5:
        card = "7";
        break;
      case 6:
        card = "6";
        break;
      case 7:
        card = "5";
        break;
      case 8:
        card = "4";
        break;
      case 9:
        card = "3";
        break;
      case 10:
        card = "2";
        break;
      case 11:
        card = "1";
        break;
      case 12:
        card = "Ace";
        break;
    }

    // print the name of the card and its suit
    System.out.println( "You picked the " + card + " of " + suit + ".") ;
    
  }
}

