//////////////
//// Hannah Roach
//// 10/4/18
//// CSE02 hw05
///
///
////  ask the user how many times it should generate hands
////  for each hand, generate five random numbers from 1 to 52
////  check if this set of five cards belongs to one of the four hands: 
////  four-of-a-kind, three-of-a-kind, two-pair, one-pair
////  If it belongs to one of the hands, the counter for this hand should be incremented by one
////  After all hands have been generated, calculate the probabilities of each of the four hands described above....
////  by computing the number of occurrences divided by the total number of hands


import java.util.Scanner;
import java.util.Random;
public class Hw05 {
 public static void main(String[] args) {
    
  Scanner scan = new Scanner( System.in );
     
  // Records course number
     System.out.println ("How many times should hands be generated? ");
     while (!scan.hasNextInt()) {
     System.out.println("Please enter an integer"); 
     scan.next(); 
     }
     int numHands = scan.nextInt();
     
 // pairs for card1     
     int pair1And2 = 0;
     int pair1And3 = 0;
     int pair1And4 = 0;
     int pair1And5 = 0;
 // pairs for card2
     int pair2And3 = 0;
     int pair2And4 = 0;
     int pair2And5 = 0;
 // pairs for card3
     int pair3And4 = 0;
     int pair3And5 = 0;
 //pairs for card4
     int pair4And5 = 0;
 // three-of-a-kind for card1
     int three123 = 0;
     int three124 = 0;
     int three125 = 0;
     int three134 = 0;
     int three135 = 0;
     int three145 = 0;
 // three-of-a-kind for card2
     int three234 = 0;
     int three235 = 0;
     int three245 = 0;
 // three-of-a-kind for card3    
     int three345 = 0;
 // four-of-a-kind
     int four1234 = 0;
     int four1235 = 0;
     int four1245 = 0;
     int four1345 = 0;
     int four2345 = 0;
 // types of pairs
     double onePair = 0;
     double twoPairs = 0;
     double threeOfAKind = 0;
     double fourOfAKind = 0;
     double fullHouse = 0;
 
     
     int counter = 0;
//initialize the individual card variables
      int card1 = 0;
      int card2 = 0;
      int card3 = 0;
      int card4 = 0;
      int card5 = 0;
      
      int detectFour = 0;
      int detectThree = 0;

     while(counter <= numHands) {
 
      
 //identify the card numbers
      int aCard1 = (int)(Math.random() * 52 + 1);
      int aCard2 = (int)(Math.random() * 52 + 1);
      while (aCard1 == aCard2) {
       aCard2 = (int)(Math.random() * 52 + 1);
      }
      int aCard3 = (int)(Math.random() * 52 + 1);
      while ((aCard2 == aCard3) || (aCard1 == aCard3)) {
       aCard3 = (int)(Math.random() * 52 + 1);
      }
      int aCard4 = (int)(Math.random() * 52 + 1);
      while ((aCard3 == aCard4) || (aCard2 == aCard4) || (aCard1 == aCard4)) {
       aCard4 = (int)(Math.random() * 52 + 1);
      }
      int aCard5 = (int)(Math.random() * 52 + 1);
      while ((aCard4 == aCard5) || (aCard3 == aCard5) || (aCard2 == aCard5) || (aCard1 == aCard5)) {
       aCard5 = (int)(Math.random() * 52 + 1);
      }
      
      
 //change the card numbers to 13 or less to be like a real stack of cards
      card1 = aCard1 % 13 + 1;
      card2 = aCard2 % 13 + 1;
      card3 = aCard3 % 13 + 1;
      card4 = aCard4 % 13 + 1;
      card5 = aCard5 % 13 + 1;
       

 // four-of-a-kind     
      if ((card1 == card2) && (card2 == card3) && (card3 == card4) && (card1 != card5)) {
       fourOfAKind++;
       detectFour = card1;
      }
      if ((card1 == card2) && (card2 == card3) && (card3 == card5) && (card1 != card4)) {
       fourOfAKind++;
       detectFour = card1;
      }
      if ((card1 == card2) && (card2 == card4) && (card4 == card5) && (card1 != card3)) {
       fourOfAKind++;
       detectFour = card1;
      }
      if ((card1 == card3) && (card3 == card4) && (card4 == card5) && (card1 != card2)) {
       fourOfAKind++;
       detectFour = card1;
      }
      if ((card2 == card3) && (card3 == card4) && (card4 == card5) && (card2 != card1)) {
       fourOfAKind++;
       detectFour = card2;
      }    
      
 // three-of-a-kind for card1     
      if ((card1 == card2) && (card2 == card3) && (card3 != card4) && (card3 != card5) && (card4 != card5)) {
       threeOfAKind++;
       detectThree = card1;
      }
      else if ((card1 == card2) && (card2 == card4) && (card2 != card3) && (card2 != card5) && (card3 != card5)) {
       threeOfAKind++;
       detectThree = card1;
      }
      else if ((card1 == card2) && (card2 == card5) && (card2 != card3) && (card2 != card4) && (card3 != card4)) {
       threeOfAKind++;
       detectThree = card1;
      }
      else if ((card1 == card3) && (card3 == card4) && (card1 != card2) && (card1 != card5) && (card2 != card5)) {
       threeOfAKind++;
       detectThree = card1;
      }
      else if ((card1 == card3) && (card3 == card5) && (card1 != card2) && (card1 != card4) && (card2 != card4)) {
       threeOfAKind++;
       detectThree = card1;
      }
      else if ((card1 == card4) && (card4 == card5) && (card1 != card2) && (card1 != card3) && (card2 != card3)) {
       threeOfAKind++;
       detectThree = card1;
      }
       // three-of-a-kind for card2     
      if ((card2 == card3) && (card3 == card4) && (card2 != card1) && (card2 != card5) && (card1 != card5)) {
       threeOfAKind++;
       detectThree = card2;
      }
      else if ((card2 == card3) && (card3 == card5) && (card2 != card1) && (card2 != card4) && (card1 != card4)) {
       threeOfAKind++;
       detectThree = card2;
      }
      else if ((card2 == card4) && (card4 == card5) && (card2 != card1) && (card2 != card4) && (card1 != card4)) {
       threeOfAKind++;
       detectThree = card2;
      }
      
 // three-of-a-kind for card3
      if ((card3 == card4) && (card4 == card5) && (card3 != card1) && (card3 != card2) && (card1 != card2)) {
       threeOfAKind++;
       detectThree = card3;
      }
      
 /////////////////////////////////////////////////////////////////////////////
       
//  determine one and two pairs     
   if ( (card1 == card2) && (card1 != detectFour) && (card1 != detectThree)) {
          if (((card3 == card4 || card3 == card5) && (card3 != detectThree)) || ((card4 == card5) && (card4 != detectThree))) {
            twoPairs++;
          }
          else {
            onePair++;
          }
        }
        else if ( (card1 == card2) && (card1 != detectFour) && (card1 != detectThree)) {
          if (((card2 == card4 || card2 == card5) && (card2 != detectThree)) || ((card4 == card5) && (card4 != detectThree))) {
            twoPairs++;
          }
          else {
            onePair++;
          }
        }
        else if ( (card1 == card4) && (card1 != detectFour) && (card1 != detectThree)) {
          if (((card2 == card3 || card2 == card5) && (card2 != detectThree)) || ((card3 == card5) && (card3 != detectThree))) {
            twoPairs++;
          }
          else {
            onePair++;
          }
        }
        else if ( (card1 == card5) && (card1 != detectFour) && (card1 != detectThree)) {
          if (((card2 == card3 || card2 == card4) && (card2 != detectThree)) || ((card3 == card4) && (card3 != detectThree))) {
            twoPairs++;
          }
          else {
            onePair++;
          }
        }
        else if ( (card2 == card3) && (card2 != detectFour) && (card2 != detectThree) ) {
          if (((card1 == card4 || card1 == card5) && (card1 != detectThree)) || ((card4 == card5) && (card4 != detectThree))) {
            twoPairs++;
          }
          else {
            onePair++;
          }
        }
        else if ( (card2 == card4) && (card2 != detectFour) && (card2 != detectThree)) {
          if (((card1 == card3 || card1 == card5) && (card1 != detectThree)) || ((card3 == card5) && (card3 != detectThree))) {
            twoPairs++;
          }
          else {
            onePair++;
          }
        }
        else if ( (card2 == card5) && (card2 != detectFour) && (card2 != detectThree)) {
          if (((card1 == card3 || card1 == card4) && (card1 != detectThree)) || ((card3 == card5) && (card3 != detectThree))) {
            twoPairs++;
          }
          else {
            onePair++;
          }
        }
        else if ( (card3 == card4) && (card3 != detectFour) && (card3 != detectThree)) {
          if (((card1 == card2 || card1 == card5) && (card1 != detectThree)) || ((card2 == card5) && (card2 != detectThree))) {
            twoPairs++;
          }
          else {
            onePair++;
          }
        }
        else if ( (card3 == card5) && (card3 != detectFour) && (card3 != detectThree)) {
          if (((card1 == card2 || card1 == card4) && (card1 != detectThree)) || ((card2 == card4) && (card2 != detectThree))) {
            twoPairs++;
          }
          else {
            onePair++;
          }
        }
        else if ( (card4 == card5) && (card4 != detectFour) && (card4 != detectThree)) {
          if (((card1 == card2 || card1 == card3) && (card1 != detectThree)) || ((card2 == card3) && (card2 != detectThree))) {
            twoPairs++;
          }
          else{
            onePair++;
          }
        }
      
/////////////////////////////////////////////////////////////////////////////
      counter++; // so that the while loop runs the number of times the user inputed
     } // end of while loop
     
     
/////////////////////////////////////////////////////////////////////////////

     
     double prob4OfAKind = fourOfAKind / numHands;
     double prob3OfAKind = threeOfAKind / numHands;
     double prob2Pairs = twoPairs / numHands;
     double prob1Pair = onePair / numHands;
 
 // print the output statements
     System.out.println("The number of loops: " + numHands);
     System.out.println("The probability of Four-of-a-kind: " + prob4OfAKind);
     System.out.println("The probability of Three-of-a-kind: " + prob3OfAKind);
     System.out.println("The probability of Two-pair: " + prob2Pairs);
     System.out.println("The probability of One-pair: " + prob1Pair);      
     
 }
}
  
