//////////////
//// Hannah Roach
//// 10/4/18
//// CSE02 User Input 
///
//// ask the user to enter information relating to a course they are currently taking, the course number, 
//// department name, the number of times it meets in a week, the time the class starts, the instructor name, and the number of students.
//// check each item to make sure that what the user enters is actually of the correct type
//// if it's not true, use an infinite loop to ask again

import java.util.Scanner;
public class UserInput {
  public static void main (String[] args) {
    Scanner scan = new Scanner(System.in);
    
    int courseNum = 0;
    String deptName;
    int timesMeet = 0;
    String timeStart;
    String instructorName;
    int numStudents = 0;
   
 // Records course number
    System.out.println ("Enter the course number ");
    while (!scan.hasNextInt()) {
    System.out.println("Please enter an integer"); 
    scan.next(); 
    }
    courseNum = scan.nextInt();
    
 // Records department name
    System.out.println ("Enter the department name ");
    while (!scan.hasNext() || scan.hasNextInt()) {
    System.out.println("Please enter a String"); 
    scan.next(); 
    }
    deptName = scan.next();
    
 // Records the number of times the course starts in a week
    System.out.println ("Enter the number of times the course meets in a week ");
    while (!scan.hasNextInt()) {
    System.out.println("Please enter an integer"); 
    scan.next(); 
    }
    timesMeet = scan.nextInt();
    
 // Records time the class begins 
    System.out.println ("Enter the time the class starts as a String ");
    while (!scan.hasNext() || scan.hasNextInt()) {
    System.out.println("Please enter a String"); 
    scan.next(); 
    }
    timeStart = scan.next();
    
 // Records the name of the instructor 
    System.out.println ("Enter the name of the instructor ");
    while (!scan.hasNext() || scan.hasNextInt()) {
    System.out.println("Please enter a String"); 
    scan.next(); 
    }
    instructorName = scan.next();
    
 // Records number of students
    System.out.println ("Enter the number of students ");
    while (!scan.hasNextInt()) {
    System.out.println("Please enter an integer"); 
    scan.next(); 
    }
    numStudents = scan.nextInt();
    
    System.out.printf(courseNum + "\n" + deptName + "\n" + timesMeet + "\n" + timeStart + "\n" + instructorName + "\n" + numStudents + "\n");    
    
  }  
}  