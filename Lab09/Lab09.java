//////////////
//// Hannah Roach
//// 11/15/18
//// CSE002 Lab09 Passing Arrays Inside Methods
///
//// 1. copy() method creates and returns an array.
//// 2. inverter() method modifies/inverts the memory pointed to by the array input.
//// 3. inverter2() takes the copy and inverts the copy.
//// 4. print() prints all members of any array.
//////////////
//// declar literal integer arrays array0, array1, and array2 in main method
//// pass array0 to inverter() and print with print(). 
//// pass array1 to inverter2() and print with print().
//// pass array2 to inverter2() and assign the output to array3. Print array3 with print().
//////////////
//// 1. reverse array0
//// 2. array1 should appear unchanged
//// 3. array3 is a reversed copy of array2

import java.util.Arrays;
public class Lab09 {
  public static void main(String[]args) {
    
    int array0[] = new int [8];               // make two copies in copy() method
                                              // then pass to inverter() method
                                              // then from inverter() method to print() method
    int array1[] = new int [8];
    int array2[] = new int [8];
    int array3[] = new int [8];
    
    int copy;
    int inverter;
    int inverter2; 
    int print;
    
    System.out.println(copy(array0));
    
    //System.out.println(print(array0[8], array1[8], array3[8])); //this doesn't work
      
      
  } // end of main method

  
  
  /////  1  //////
  
  
// 1.
//// method copy() that accepts an integer array as input and returns an array.
//// copy() should declare and allocate a new integer array that is the same length as the input
//// use a for-loop to copy each member of the input to the same member in the new array.
//// copy should return the new array as an output.
  public static int[] copy(int[] array0) {
    
    int[] array1 = array0;
    int[] array2 = array0;
    
    for (int i = 0; i < array0.length; i++) {
      array1[i] = array0[i];
      array2[i] = array0[i];
    }
    
    //int[] ar[] = new int[2];
    //ar[0]= array1;
    //ar[1]= array2;
    
    return new int[] {array1[0],array2[0]}; //Pair<array1, array2>(8);
    //return ar;
    
  } // end of copy method
  
  
  
  /////  2  //////
  
  

// 2.
//// method inverter() reverses the order of an array.
//// accepts an array of integers as input and returns void.
//// systematically swaps the first member of the input array with the last member, 
//// second with the second-to-last member, and so on.
  public static void inverter (int[] array0[]) {
    
    for(int i = 0; i < array0.length / 2; i++) {
      int[] temp = array0[i];
      array0[i] = array0[array0.length - i - 1];
      array0[array0.length - i - 1] = temp;
    }
    
  } // end of inverter method
  
  
  
  
  /////  3  /////
  
  
  
// 3.
//// inverter2() uses copy() to make a copy of the input array (call the copy() method).
//// then in uses a copy of the code from inverter to invert members of the copy (call inverter() method).
//// returns the inverted copy.
  public static int[] inverter2 (/*int[] array[], int array2[] */ int[] array0[]) {
    
    int copy;

    int[] array1 = array0[8];
// invert array1, pass it to print() method
    for(int i = 0; i < (array1.length / 2); i++) {
      int[] temp = array1;
      array1[i] = array1[array1.length - i - 1];
      array1[array1.length - i - 1] = temp[i];
    }
    
    int[] array2 = array0[8];
// invert array2 and make it array3
    for(int i = 0; i < (array2.length / 2); i++) {
      int temp = array2[i];
      array2[i] = array2[array2.length - i - 1];
      array2[array2.length - i - 1] = temp;
    }
    int[] array3 = array2;
    
    return new int[] { array1[8], array3[8] };
    
   // return new Pair<array1, array3>(8);
   // return array2;
   // return array3;
    
  } // end of inverter2 method
  
  
  
  
  /////  4  /////
  
  
// 4. 
//// print () accepts any integer array as an input and returns void.
//// use a for-loops to print all members of the array.
  public static void print (int[] array0[], int[] array1[], int[] array3[]) {
    
    int inverter;
    
// print array0    
    for (int i = 0; i < array0.length - 1; i++) {
      System.out.print(array0[i] + " ");
    }
    
    int inverter2;
    
// print array1   
    for (int i = 0; i < array1.length - 1; i++) {
      System.out.print(array1[i] + " ");
    }
    
// print array3
    for (int i = 0; i < array3.length - 1; i++) {
      System.out.print(array3[i] + " ");
    }
    
    
  } // end of print method

  
} // end of public class









